<?php

namespace Drupal\content_tamper\Form;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Plugin\DataType\EntityAdapter;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\tamper\Adapter\TamperableComplexDataAdapter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\tamper\TamperManagerInterface;

/**
 * Provides a form to apply tamper plugins to nodes.
 *
 * @todo - lots:
 *  - Coding standards
 *  - Make available fields safer
 *  - Handle multiple values
 *  - Handle field storage where value is empty
 *  - Messaging and exit process.
 */
class TamperContentMultipleNodes extends FormBase {

  // Form fields.
  const VAR_TAMPER_ID = 'tamper_id';
  const VAR_PLUGIN_CONFIGURATION = 'plugin_configuration';
  const VAR_SOURCE = 'source';

  /**
   * The array of nodes to tamper.
   *
   * @var string[][]
   */
  protected $nodeInfo = [];

  /**
   * The temp store factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * Drupal\tamper\TamperManagerInterface definition.
   *
   * @var \Drupal\tamper\TamperManagerInterface
   */
  protected $tamperManager;

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $fieldManager;

  /**
   * TamperContentMultipleNodes constructor.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $manager
   * @param \Drupal\tamper\TamperManagerInterface $plugin_manager_tamper
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory, EntityTypeManagerInterface $manager, TamperManagerInterface $plugin_manager_tamper, EntityFieldManagerInterface $field_manager) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->storage = $manager->getStorage('node');
    $this->tamperManager = $plugin_manager_tamper;
    $this->fieldManager = $field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.tamper'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_tamper_node_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->nodeInfo = $this->tempStoreFactory->get('content_tamper_node_confirm')->get(\Drupal::currentUser()->id());

    $this->addSourceForm($form, $form_state);
    $this->addTamperForm($form, $form_state);

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * Ajax callback.
   *
   * Returns the plugin configuration form from an ajax request.
   *
   * @param array $form
   *   Drupal form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state interface.
   *
   * @return array
   *   Plugin form.
   */
  public function getPluginForm(array $form, FormStateInterface $form_state) {
    return $form[self::VAR_PLUGIN_CONFIGURATION];
  }

  /**
   * Add the functionality to select the source data from the entities.
   *
   * @param array $form
   *   Drupal form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state interface.
   */
  protected function addSourceForm(array &$form, FormStateInterface $form_state) {
    $node_fields = $this->fieldManager->getFieldStorageDefinitions('node');

    $options = [];
    foreach ($node_fields as $definition) {
      /** @todo: Implement a blacklist for ids, revision, etc? */
      $options[$definition->getName()] = (string) $definition->getLabel();
    }

    $form[self::VAR_SOURCE] = [
      '#type' => 'select',
      '#title' => $this->t('Field source'),
      '#description' => $this->t('The field that contains the data to tamper.'),
      '#options' => $options,
    ];
  }

  /**
   * Add the functionality to configure a tamper plugin to apply.
   *
   * @param array $form
   *   Drupal form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state interface.
   */
  protected function addTamperForm(array &$form, FormStateInterface $form_state) {
    $form[self::VAR_TAMPER_ID] = [
      '#type' => 'select',
      '#title' => $this->t('The plugin to add'),
      '#options' => $this->getPluginOptions(),
      '#required' => TRUE,
      '#ajax' => [
        'callback' => self::class . '::getPluginForm',
        'wrapper' => 'plugin-config',
      ],
    ];

    $form[self::VAR_PLUGIN_CONFIGURATION] = [
      '#type' => 'container',
      '#tree' => TRUE,
      '#attributes' => ['id' => ['plugin-config']],
    ];

    if (($tamper_id = $form_state->getValue(self::VAR_TAMPER_ID))) {
      $plugin = $this->tamperManager->createInstance($tamper_id);
      $form[self::VAR_PLUGIN_CONFIGURATION]['description'] = [
        '#markup' => $plugin->getPluginDefinition()['description'],
      ];

      $subform_state = SubformState::createForSubform($form[self::VAR_PLUGIN_CONFIGURATION], $form, $form_state);
      $form[self::VAR_PLUGIN_CONFIGURATION] = $plugin->buildConfigurationForm($form[self::VAR_PLUGIN_CONFIGURATION], $subform_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getValue('submit') || empty($this->nodeInfo)) {
      $form_state->setRedirect('system.admin_content');
      return;
    }

    $source = $form_state->getValue(self::VAR_SOURCE);
    $plugin = $this->tamperManager->createInstance(
      $form_state->getValue(self::VAR_TAMPER_ID),
      $form_state->getValue(self::VAR_PLUGIN_CONFIGURATION)
    );

    /** @var \Drupal\node\NodeInterface[] $nodes */
    $nodes = $this->storage->loadMultiple(array_keys($this->nodeInfo));

    /**
     * @todo: At the moment, this is only working for fields that return store
     * their data in value, and will only work for single value fields as the
     * FieldListItem will return just the first item. There is also no check
     * for how the plugin handle multiple values - we should either loop here
     * or ignore these plugins if they expect multiple.
     */

    foreach ($nodes as $node) {
      if ($node->hasField($source) && ($data = $node->{$source}->value)) {
        // Prepare the adapters.
        $entity_adapter = EntityAdapter::createFromEntity($node);
        $tamperable_item = new TamperableComplexDataAdapter($entity_adapter);

        $new_data = $plugin->tamper($data, $tamperable_item);
        $node->set($source, $new_data);
        $node->save();
      }
    }

    $form_state->setRedirect('system.admin_content');
  }

  /**
   * Get the tamper plugin options.
   *
   * @return array
   *   List of tamper plugin groups, keyed by group, where the value is another
   *   array of plugin labels keyed by plugin id.
   */
  protected function getPluginOptions() {
    $plugin_options = array_map(function ($grouped_plugins) {
      $group_options = [];
      foreach ($grouped_plugins as $id => $plugin_definition) {
        $group_options[$id] = $plugin_definition['label'];
      }
      return $group_options;
    }, $this->tamperManager->getGroupedDefinitions());

    return $plugin_options;
  }

}
